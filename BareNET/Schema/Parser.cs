﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace BareNET.Schema
{
    public static class Parser
    {
        public static IEnumerable<UserType> Parse(Scanner scanner)
        {
            while (!scanner.EndOfStream)
            {
                var next = Lexer.Next(scanner);
                if (!next.HasValue) break;

                yield return ParseSchemaType(next.Value.token, scanner);
            }
        }

        private static UserType ParseSchemaType(Token token, Scanner scanner)
        {
            return token switch
            {
                Token.Type => ParseUserType(scanner),
                _ => throw new FormatException($"Expected token 'type' but got {token}")
            };
        }

        private static bool Is_unallowed_typename_letter(char c) => !char.IsDigit(c) && !(c >= 'A' && c <= 'Z') && !(c >= 'a' && c <= 'z');

        private static UserType ParseUserType(Scanner scanner)
        {
            var word = Lexer.Next(scanner);
            if (!word.HasValue) throw new FormatException("Expected WORD but got nothing");

            if (word.Value.token != Token.Word) throw new FormatException($"Expected WORD but got {word.Value}");
            var name = word.Value.value;
            if (char.IsLower(name, 0)) throw new FormatException("Type must start with uppercase letter");
            if (name.Any(Is_unallowed_typename_letter)) throw new FormatException("Only letter and digits are allowed for type names");

            var nextWord = Lexer.Next(scanner);
            if (!nextWord.HasValue) throw new FormatException("Expected type specification but got nothing");

            if (nextWord.Value.token == Token.Enum) return new EnumUserType(name, ParseEnumType(scanner));

            scanner.Unread(); // Rollback if read word is not an enum token
            return new NamedUserType(name, ParseNonEnumType(scanner));

        }

        private static NonEnumType ParseNonEnumType(Scanner scanner)
        {
            var next = Lexer.Next(scanner);
            if (!next.HasValue) throw new FormatException("Missing non-enum-type");
            var (token, value) = next.Value;

            return token switch
            {
                Token.UInt => new PrimitiveType(TypeKind.UInt, null),
                Token.U8 => new PrimitiveType(TypeKind.U8, null),
                Token.U16 => new PrimitiveType(TypeKind.U16, null),
                Token.U32 => new PrimitiveType(TypeKind.U32, null),
                Token.U64 => new PrimitiveType(TypeKind.U64, null),
                Token.Int => new PrimitiveType(TypeKind.Int, null),
                Token.I8 => new PrimitiveType(TypeKind.I8, null),
                Token.I16 => new PrimitiveType(TypeKind.I16, null),
                Token.I32 => new PrimitiveType(TypeKind.I32, null),
                Token.I64 => new PrimitiveType(TypeKind.I64, null),
                Token.F32 => new PrimitiveType(TypeKind.F32, null),
                Token.F64 => new PrimitiveType(TypeKind.F64, null),
                Token.Bool => new PrimitiveType(TypeKind.Bool, null),
                Token.String => new PrimitiveType(TypeKind.String, null),
                Token.Void => new PrimitiveType(TypeKind.Void, null),
                Token.Optional => ParseOptionalType(scanner),
                Token.Data => ParseDataType(scanner),
                Token.Map => ParseMapType(scanner),
                Token.List => ParseListType(scanner),
                Token.Union => ParseUnionType(scanner),
                Token.Struct => ParseStructType(scanner),
                Token.Word => new UserTypeName(value),
                _ => throw new FormatException($"Unexpected token {token}")
            };
        }

        private static OptionalType ParseOptionalType(Scanner scanner)
        {
            var leftAngle = Lexer.Next(scanner);
            if (!leftAngle.HasValue || leftAngle.Value.token != Token.LeftAngle) throw new FormatException("Expected '<' after 'optional'");
            var type = ParseNonEnumType(scanner);
            var rightAngle = Lexer.Next(scanner);
            if (!rightAngle.HasValue || rightAngle.Value.token != Token.RightAngle) throw new FormatException("Missing '>' after 'optional<'");
            return new OptionalType(type);
        }

        private static PrimitiveType ParseDataType(Scanner scanner)
        {
            var leftBracket = Lexer.Next(scanner);
            if (!leftBracket.HasValue || leftBracket.Value.token != Token.LeftBracket)
            {
                if (leftBracket.HasValue) scanner.Unread();
                return new PrimitiveType(TypeKind.Data, null);
            }

            var digit = Lexer.Next(scanner);
            if (!digit.HasValue || digit.Value.token != Token.Digit) throw new FormatException("Expected a number after 'data['");
            var length = ParseLength(digit.Value.value, "data");

            var rightBracket = Lexer.Next(scanner);
            if (!rightBracket.HasValue || rightBracket.Value.token != Token.RightBracket) throw new FormatException("Missing ']' after 'data['");

            return new PrimitiveType(TypeKind.Data, length);
        }

        private static ulong ParseLength(string value, string dataType)
        {
            var length = ulong.Parse(value);
            if (length == 0) throw new InvalidDataException($"Length of '{dataType}' must be at least 1");
            return length;
        }

        private static MapType ParseMapType(Scanner scanner)
        {
            var leftAngleKey = Lexer.Next(scanner);
            if (!leftAngleKey.HasValue || leftAngleKey.Value.token != Token.LeftAngle) throw new FormatException("Expected '<' after 'map'");

            var keyType = ParseNonEnumType(scanner);

            var rightAngleKey = Lexer.Next(scanner);
            if (!rightAngleKey.HasValue || rightAngleKey.Value.token != Token.RightAngle) throw new FormatException("Expected '>' after 'map<'");

            var leftAngleVal = Lexer.Next(scanner);
            if (!leftAngleVal.HasValue || leftAngleVal.Value.token != Token.LeftAngle) throw new FormatException("Expected '<' after 'map<key_type>'");

            var valueType = ParseNonEnumType(scanner);

            var rightAngleVal = Lexer.Next(scanner);
            if (!rightAngleVal.HasValue || rightAngleVal.Value.token != Token.RightAngle) throw new FormatException("Expected '>' after 'map<key_type><'");

            return new MapType(keyType, valueType);
        }

        private static ListType ParseListType(Scanner scanner)
        {
            var leftAngle = Lexer.Next(scanner);
            if (!leftAngle.HasValue || leftAngle.Value.token != Token.LeftAngle) throw new FormatException("Expected '<' after 'list'");

            var listType = ParseNonEnumType(scanner);

            var rightAngle = Lexer.Next(scanner);
            if (!rightAngle.HasValue || rightAngle.Value.token != Token.RightAngle) throw new FormatException("Expected '>' after 'list<'");

            var next = Lexer.Next(scanner);
            if (next.HasValue && next.Value.token == Token.LeftBracket)
            {

                next = Lexer.Next(scanner);

                if (next.Value.token == Token.Digit)
                {
                    var length = ParseLength(next.Value.value, "list");
                    var rightBracket = Lexer.Next(scanner);
                    if (!rightBracket.HasValue || rightBracket.Value.token != Token.RightBracket) throw new FormatException("Missing ']' for list");
                    return new ListType(listType, length);
                }
                if (next.Value.token == Token.RightBracket) return new ListType(listType, null);
                throw new FormatException($"Unexpected {next.Value.token} at list");
            }
            else
            {
                scanner.Unread();
                return new ListType(listType, null);
            }
        }

        private static UnionType ParseUnionType(Scanner scanner)
        {
            var next = Lexer.Next(scanner);
            if (!next.HasValue || next.Value.token != Token.LeftBrace) throw new FormatException("Missing { after 'union'");

            return new(ParseUnionMembers(scanner).ToArray());
        }

        private static List<UnionMember> ParseUnionMembers(Scanner scanner)
        {
            var result = new List<UnionMember>();
            while (true)
            {
                var type = ParseNonEnumType(scanner);
                var next = Lexer.Next(scanner);
                if (!next.HasValue) throw new FormatException("Missing token after 'union {'");

                if (next.Value.token == Token.Equal)
                {
                    var digit = Lexer.Next(scanner);
                    if (!digit.HasValue || digit.Value.token != Token.Digit) throw new FormatException("Expected number after '=' for union member");
                    result.Add(new UnionMember(type, int.Parse(digit.Value.value)));
                    var nextToken = Lexer.Next(scanner);
                    if (!nextToken.HasValue) throw new FormatException("Missing '}' for union");
                    if (nextToken.Value.token == Token.LeftBrace) break;
                    if (nextToken.Value.token == Token.Pipe) continue;
                }
                else if (next.Value.token == Token.Pipe)
                {
                    result.Add(new UnionMember(type, null));
                    continue;
                }
                else if (next.Value.token == Token.RightBrace)
                {
                    result.Add(new UnionMember(type, null));
                    break;
                }

                throw new FormatException($"Unexpected token {next.Value.token} for union");
            }
            return result;
        }

        private static StructType ParseStructType(Scanner scanner)
        {
            var next = Lexer.Next(scanner);
            if (!next.HasValue || next.Value.token != Token.LeftBrace) throw new FormatException("Missing { after 'struct'");

            var fields = ParseStructFields(scanner).ToArray();
            if (fields.Length == 0) throw new FormatException("Structs must have at least 1 field");
            return new StructType(fields);
        }

        private static bool Is_unallowed_struct_field_letter(char c) => !(c >= 'a' && c <= 'z') && !(c >= 'A' && c <= 'Z');

        private static List<StructField> ParseStructFields(Scanner scanner)
        {
            var result = new List<StructField>();
            while (true)
            {
                var word = Lexer.Next(scanner);
                if (!word.HasValue || word.Value.token != Token.Word) throw new FormatException("Missing name for struct field");
                if (word.Value.value.Any(Is_unallowed_struct_field_letter)) throw new FormatException("Field name can only contains letters");

                var colon = Lexer.Next(scanner);
                if (!colon.HasValue || colon.Value.token != Token.Colon) throw new FormatException("Missing ':' for struct field");

                var type = ParseNonEnumType(scanner);
                result.Add(new StructField(word.Value.value, type));

                System.Diagnostics.Debug.WriteLine($"sum up as struct field {word.Value.value}: {type.GetType()}");

                var rightBrace = Lexer.Next(scanner);
                if (!rightBrace.HasValue) throw new FormatException("Missing '}' for struct");
                if (rightBrace.Value.token == Token.RightBrace) break;
                scanner.Unread();
            }
            return result;
        }

        private static EnumType ParseEnumType(Scanner scanner)
        {
            var leftBrace = Lexer.Next(scanner);
            if (!leftBrace.HasValue || leftBrace.Value.token != Token.LeftBrace) throw new FormatException("Missing '{' after 'enum'");
            return new EnumType(ParseEnumValues(scanner).ToArray());
        }

        private static bool Is_unallowed_enum_letter(char c) => c < 'A' || c > 'Z';

        private static List<EnumValue> ParseEnumValues(Scanner scanner)
        {
            var result = new List<EnumValue>();
            while (true)
            {
                var word = Lexer.Next(scanner);
                if (!word.HasValue || word.Value.token != Token.Word) throw new FormatException("Missing name for enum field");
                var name = word.Value.value;
                if (name.Any(c => !char.IsDigit(c) && c != '_' && Is_unallowed_enum_letter(c)))
                {
                    throw new FormatException("enum value must be all uppercase or contains numbers or '_'");
                }

                var next = Lexer.Next(scanner);
                if (!next.HasValue) throw new FormatException("Missing '}' for enum");
                if (next.Value.token == Token.Equal)
                {
                    var digit = Lexer.Next(scanner);
                    if (!digit.HasValue || digit.Value.token != Token.Digit) throw new FormatException("Missing number after '=' for enum value");
                    result.Add(new EnumValue(name, uint.Parse(digit.Value.value)));
                    next = Lexer.Next(scanner);
                    if (!next.HasValue) throw new FormatException("Missing '}' for enum");
                }
                else result.Add(new EnumValue(name, null));

                if (next.Value.token == Token.RightBrace) break;

                scanner.Unread();
            }
            return result;
        }
    }
}