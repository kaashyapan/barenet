﻿using System;
using System.IO;
using System.Text;

namespace BareNET.Schema
{
    public class Scanner : IDisposable
    {
        private readonly StreamReader _reader;
        private bool _unread;
        private string _lastValue;

        public Scanner(StreamReader reader)
        {
            _reader = reader;
        }

        public bool EndOfStream => _reader.EndOfStream;

        public char? ReadChar()
        {
            if (_unread)
            {
                _unread = _lastValue.Length != 1;
                return _lastValue[0];
            }

            char c;
            do
            {
                if (EndOfStream) return null;
                c = (char) _reader.Read();
                if (c == '#') _reader.ReadLine();
            } while (char.IsWhiteSpace(c) || char.IsControl(c) || c == '#');

            _lastValue = c.ToString();
            System.Diagnostics.Debug.WriteLine($"Consume {c}");
            return c;
        }

        public string ReadDigit(char first)
        {
            if (_unread)
            {
                _unread = false;
                return _lastValue;
            }
            var sb = new StringBuilder();
            sb.Append(first);
            while (char.IsDigit((char)_reader.Peek())) sb.Append((char)_reader.Read());
            _lastValue = sb.ToString();
            System.Diagnostics.Debug.WriteLine($"Consume {_lastValue}");
            return _lastValue;
        }

        public string ReadWord(char first)
        {
            if (_unread)
            {
                _unread = false;
                return _lastValue;
            }
            var sb = new StringBuilder();
            sb.Append(first);
            while (char.IsLetterOrDigit((char)_reader.Peek()) || (char)_reader.Peek() == '_') sb.Append((char)_reader.Read());
            _lastValue = sb.ToString();
            System.Diagnostics.Debug.WriteLine($"Consume {_lastValue}");
            return _lastValue;
        }

        public void Unread()
        {
            if (_lastValue != null)
            {
                _unread = true;
                System.Diagnostics.Debug.WriteLine($"Unread {_lastValue}");
            }
        }

        public void Dispose()
        {
            _reader.Dispose();
        }
    }
}