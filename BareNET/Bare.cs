﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BareNET
{
    public delegate byte[] BareEncoder<in T>(T value);
    public delegate (T, byte[]) BareDecoder<T>(byte[] data);

    public static class Bare
    {
        public static byte[] Encode_u8(byte value)
        {
            return new[] { value };
        }

        public static (byte,byte[]) Decode_u8(byte[] data)
        {
            return (data[0], data.Skip(1).ToArray());
        }

        public static byte[] Encode_i8(sbyte value)
        {
            return new[] { (byte)value };
        }

        public static (sbyte,byte[]) Decode_i8(byte[] data)
        {
            return ((sbyte)data[0], data.Skip(1).ToArray());
        }

        public static byte[] Encode_u16(ushort value)
        {
            return new[] { (byte)value, (byte)(value>>8) };
        }

        public static (ushort,byte[]) Decode_u16(byte[] data)
        {
            return ((ushort)(data[1]<<8 | data[0]), data.Skip(2).ToArray());
        }

        public static byte[] Encode_i16(short value)
        {
            return new[] { (byte)value, (byte)(value >> 8) };
        }

        public static (short,byte[]) Decode_i16(byte[] data)
        {
            return ((short)(data[1] << 8 | data[0]), data.Skip(2).ToArray());
        }

        public static byte[] Encode_u32(uint value)
        {
            return new[] { (byte)value, (byte)(value>>8), (byte)(value>>16), (byte)(value>>24) };
        }

        public static (uint,byte[]) Decode_u32(byte[] data)
        {
            return ((uint)(data[3]<<24 | data[2]<<16 | data[1]<<8 | data[0]), data.Skip(4).ToArray());
        }

        public static byte[] Encode_i32(int value)
        {
            return new[] { (byte)value, (byte)(value >> 8), (byte)(value >> 16), (byte)(value >> 24) };
        }

        public static (int,byte[]) Decode_i32(byte[] data)
        {
            return (data[3] << 24 | data[2] << 16 | data[1] << 8 | data[0], data.Skip(4).ToArray());
        }

        public static byte[] Encode_u64(ulong value)
        {
            return new[] { (byte)value, (byte)(value>>8), (byte)(value>>16), (byte)(value>>24), (byte)(value>>32), (byte)(value>>40), (byte)(value>>48), (byte)(value>>56) };
        }

        public static (ulong,byte[]) Decode_u64(byte[] data)
        {
            var bytes = BitConverter.IsLittleEndian ? data.Take(8) : data.Take(8).Reverse();
            return (BitConverter.ToUInt64(bytes.ToArray(), 0), data.Skip(8).ToArray());
        }

        public static byte[] Encode_i64(long value)
        {
            return new[] { (byte)value, (byte)(value >> 8), (byte)(value >> 16), (byte)(value >> 24), (byte)(value >> 32), (byte)(value >> 40), (byte)(value >> 48), (byte)(value >> 56) };
        }

        public static (long,byte[]) Decode_i64(byte[] data)
        {
            var bytes = BitConverter.IsLittleEndian ? data.Take(8) : data.Take(8).Reverse();
            return (BitConverter.ToInt64(bytes.ToArray(), 0), data.Skip(8).ToArray());
        }

        private static IEnumerable<byte> Split_value_into_7_bit_groups_least_significant_first(ulong value)
        {
            while (value >= 128)
            {
                yield return (byte)((value & 0xFF) | 0b10000000);
                value >>= 7;
            }
            yield return (byte)(value & 0xFF);
        }

        public static byte[] Encode_uint(ulong value)
        {
            return Split_value_into_7_bit_groups_least_significant_first(value).ToArray();
        }

        private static bool Most_siginifanct_bit_set(byte value) => (value & 0x80) == 0x80;

        public static (ulong,byte[]) Decode_uint(byte[] data)
        {
            var rest = data.SkipWhile(Most_siginifanct_bit_set).ToArray();
            var value =
                data
                    .TakeWhile(Most_siginifanct_bit_set)
                    .Concat(rest.Take(1))
                    .Select((bits, index) => (bits, index))
                    .Aggregate(0ul, (result, current) => ((ulong)(current.bits & 0x7F) << (7*current.index)) | result);
            return (value, rest.Skip(1).ToArray());
        }

        public static byte[] Encode_int(long value)
        {
            var v = (ulong)value << 1;
            return Split_value_into_7_bit_groups_least_significant_first(value < 0 ? ~v : v).ToArray();
        }

        public static (long,byte[]) Decode_int(byte[] data)
        {
            var (v, r) = Decode_uint(data);
            var value = (long) (v >> 1);
            return ((v & 0x01) == 0x01 ? ~value : value, r);
        }

        public static byte[] Encode_f32(float value)
        {
            if (float.IsNaN(value)) throw new ArgumentException("f32 value can't be NaN", nameof(value));
            return BitConverter.GetBytes(value);
        }

        public static (float, byte[]) Decode_f32(byte[] data)
        {
            return (BitConverter.ToSingle(data, 0), data.Skip(4).ToArray());
        }

        public static byte[] Encode_f64(double value)
        {
            if (double.IsNaN(value)) throw new ArgumentException("f64 value can't be NaN", nameof(value));
            return BitConverter.GetBytes(value);
        }

        public static (double, byte[]) Decode_f64(byte[] data)
        {
            return (BitConverter.ToDouble(data, 0), data.Skip(8).ToArray());
        }

        public static byte[] Encode_bool(bool value)
        {
            return new byte[] { (byte)(value ? 0x01 : 0x00) };
        }

        public static (bool, byte[]) Decode_bool(byte[] data)
        {
            var (v, rest) = Decode_u8(data);
            if (v > 1u) throw new ArgumentException("Received byte does not represent type bool", nameof(data));
            return (v == 1u, rest);
        }

        public static byte[] Encode_enum(Enum value)
        {
            var int_value = Convert.ToInt32(value);
            if (int_value < 0) throw new ArgumentException("Underlying value of enum must be unsigned", nameof(value));
            return Encode_uint((ulong) int_value);
        }

        public static (TEnum, byte[]) Decode_enum<TEnum>(byte[] data) where TEnum : Enum
        {
            var (value, rest) = Decode_uint(data);
            var int_value = (int) value;
            var enumType = typeof(TEnum);
            if (!Enum.IsDefined(enumType, int_value)) throw new ArgumentException($"Received value {int_value} is not a member of enum {enumType}", nameof(data));
            return ((TEnum) Enum.ToObject(enumType, int_value), rest);
        }

        public static byte[] Encode_string(string value)
        {
            return
                Encode_uint((ulong) Encoding.UTF8.GetByteCount(value))
                    .Concat(Encoding.UTF8.GetBytes(value))
                    .ToArray();
        }

        public static (string, byte[]) Decode_string(byte[] data)
        {
            var (length, rest) = Decode_uint(data);
            return (Encoding.UTF8.GetString(rest, 0, (int) length), rest.Skip((int) length).ToArray());
        }

        public static byte[] Encode_data_fixed_length(int length, byte[] value)
        {
            if (length <= 0) throw new ArgumentException("Length must be at least 1", nameof(length));
            if (value.Length < length) throw new ArgumentException("Given data is less than needed data length", nameof(value));
            return value;
        }

        public static (byte[], byte[]) Decode_data_fixed_length(int length, byte[] data)
        {
            return (data.Take(length).ToArray(), data.Skip(length).ToArray());
        }

        public static byte[] Encode_data(byte[] value)
        {
            return Encode_uint((ulong) value.Length)
                .Concat(value)
                .ToArray();
        }

        public static (byte[], byte[]) Decode_data(byte[] data)
        {
            var (length, rest) = Decode_uint(data);
            return (rest.Take((int)length).ToArray(), rest.Skip((int)length).ToArray());
        }

        public static byte[] Encode_optional<T>(T? value, BareEncoder<T> encoder) where T : struct
        {
            return value.HasValue
                ? Encode_bool(true)
                    .Concat(encoder(value.Value))
                    .ToArray()
                : Encode_bool(false);
        }

        public static byte[] Encode_optional_ref<T>(T value, BareEncoder<T> encoder) where T : class
        {
            return value != null
                ? Encode_bool(true)
                    .Concat(encoder(value))
                    .ToArray()
                : Encode_bool(false);
        }

        public static (T?, byte[]) Decode_optional<T>(byte[] data, BareDecoder<T> decoder) where T : struct
        {
            var (hasValue, rest) = Decode_bool(data);
            return hasValue ? ((T?, byte[])) decoder(rest) : (null, rest);
        }

        public static (T, byte[]) Decode_optional_ref<T>(byte[] data, BareDecoder<T> decoder) where T : class
        {
            var (hasValue, rest) = Decode_bool(data);
            return hasValue ? decoder(rest) : (null!, rest);
        }

        private static byte[] Encode_values<T>(IEnumerable<T> value, BareEncoder<T> encoder)
        {
            return value
                .SelectMany(v => encoder(v))
                .ToArray();
        }

        public static byte[] Encode_list_fixed_length<T>(int length, IEnumerable<T> value, BareEncoder<T> encoder)
        {
            if (length <= 0) throw new ArgumentException("Length must be at least 1", nameof(length));
            var list = value.ToList();
            if (list.Count < length) throw new ArgumentException("Given data is less than needed data length", nameof(value));

            return Encode_values(list.Take(length), encoder);
        }

        public static (IEnumerable<T>, byte[]) Decode_list_fixed_length<T>(int length, byte[] data, BareDecoder<T> decoder)
        {
            var result = new List<T>();
            var rest = data;
            for (var i = 0; i < length; i++)
            {
                var (value, r) = decoder(rest);
                result.Add(value);
                rest = r;
            }

            return (result, rest);
        }

        public static byte[] Encode_list<T>(IEnumerable<T> value, BareEncoder<T> encoder)
        {
            var list = value.ToList();
            return Encode_uint((ulong) list.Count)
                .Concat(Encode_values(list, encoder))
                .ToArray();
        }

        public static (IEnumerable<T>, byte[]) Decode_list<T>(byte[] data, BareDecoder<T> decoder)
        {
            var (length, rest) = Decode_uint(data);
            return Decode_list_fixed_length((int) length, rest, decoder);
        }

        public static byte[] Encode_map<TKey, TValue>(Dictionary<TKey, TValue> map, BareEncoder<TKey> keyEncoder, BareEncoder<TValue> valueEncoder)
        {
            return Encode_uint((ulong) map.Count)
                .Concat(map.SelectMany(pair => keyEncoder(pair.Key).Concat(valueEncoder(pair.Value))))
                .ToArray();
        }

        public static (Dictionary<TKey, TValue>, byte[]) Decode_map<TKey, TValue>(byte[] data, BareDecoder<TKey> keyDecoder, BareDecoder<TValue> valueDecoder)
        {
            var (length, rest) = Decode_uint(data);
            var result = new Dictionary<TKey, TValue>();
            for (var i = 0ul; i < length; i++)
            {
                var (key, keyRest) = keyDecoder(rest);
                var (value, valueRest) = valueDecoder(keyRest);
                result.Add(key, value);
                rest = valueRest;
            }

            return (result, rest);
        }

		public static byte[] Encode_union<T>(T value, Dictionary<Type, (uint, BareEncoder<T>)> encoders)
		{
            var type = value.GetType();
            foreach (var t in type.GetInterfaces().Append(type))
            {
                if (!encoders.ContainsKey(t)) continue;
                var (identifier, encoder) = encoders[t];
                return Encode_union_case(identifier, encoder(value));
            }

            throw new ArgumentException($"Missing identifier of type '{type}'", nameof(encoders));
        }

		public static byte[] Encode_union_case(uint identifier, byte[] data)
		{
			return Encode_uint(identifier)
                .Concat(data)
                .ToArray();
		}

		public static (T, byte[]) Decode_union<T>(byte[] data, Dictionary<uint, BareDecoder<T>> decoders)
		{
			return Decode_union(
				data,
				identifier =>
                {
					if (!decoders.ContainsKey(identifier)) {
						throw new ArgumentException($"Missing encoder for identifier {identifier}", nameof(decoders));
					}
					return decoders[identifier];
				});
		}

		public static (T, byte[]) Decode_union<T>(byte[] data, Func<uint, BareDecoder<T>> decoder)
		{
			var (identifier, rest) = Decode_uint(data);
			return decoder((uint) identifier)(rest);
		}
    }
}
