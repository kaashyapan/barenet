﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BareNET
{
    internal readonly struct UnionCase<T>
    {
        public readonly uint Identifier;
        public readonly BareEncoder<T> Encoder;
        public readonly BareDecoder<T> Decoder;

        public UnionCase(uint identifier, BareEncoder<T> encoder, BareDecoder<T> decoder)
        {
            Identifier = identifier;
            Encoder = encoder;
            Decoder = decoder;
        }
    }

    public class Union<T>
    {
        private readonly Dictionary<Type, UnionCase<T>> _cases;

        public static Union<T> Register()
        {
            return new();
        }

        private Union()
        {
            _cases = new Dictionary<Type, UnionCase<T>>();
        }

        public Union<T> With_Case<TCase>(BareEncoder<T> encoder, BareDecoder<T> decoder)
        {
            var identifier = _cases.Count > 0 ? _cases.Max(_ => _.Value.Identifier) + 1 : 0;
            _cases.Add(typeof(TCase), new UnionCase<T>(identifier, encoder, decoder));
            return this;
        }

        public Union<T> With_Case_identified_by<TCase>(uint identifier, BareEncoder<T> encoder, BareDecoder<T> decoder)
        {
            if (_cases.Any(_ => _.Value.Identifier == identifier)) throw new ArgumentException($"Identifier {identifier} already registered", nameof(identifier));
            _cases.Add(typeof(TCase), new UnionCase<T>(identifier, encoder, decoder));
            return this;
        }

        public static implicit operator Dictionary<uint, BareDecoder<T>>(Union<T> union)
        {
            return union._cases
                .Values
                .ToDictionary(_ => _.Identifier, _ => _.Decoder);
        }

        public static implicit operator Dictionary<Type, (uint, BareEncoder<T>)>(Union<T> union)
        {
            return union._cases.ToDictionary(_ => _.Key, _ => (_.Value.Identifier, _.Value.Encoder));
        }
    }
}